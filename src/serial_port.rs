use serial::{BaudRate, CharSize, FlowControl, Parity, SerialPort, StopBits, SystemPort};
use std::time::Duration;

pub fn open(path: &str) -> Result<SystemPort, serial::Error> {
    let mut port = serial::open(path)?;

    // TODO(José): We should allow configurations through command line,
    // at least for the baudrate.
    port.reconfigure(&|settings| {
        settings.set_baud_rate(BaudRate::BaudOther(921_600))?;
        settings.set_char_size(CharSize::Bits8);
        settings.set_parity(Parity::ParityNone);
        settings.set_stop_bits(StopBits::Stop1);
        settings.set_flow_control(FlowControl::FlowNone);
        Ok(())
    })?;

    if cfg!(windows) {
        // FIXME(José): When passing 0 to windows, the function blocks!
        port.set_timeout(Duration::from_millis(1))?;
    } else if cfg!(unix) {
        port.set_timeout(Duration::from_millis(0))?;
    }

    Ok(port)
}
