mod elf_parser;
mod serial_port;
mod simulator;
mod uploader;

use clap::{Parser, Subcommand};
use elf_parser::ElfParser;
use std::{
    fs,
    process::{Child, Command, Stdio},
    thread,
    time::Duration,
};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
#[clap(propagate_version = true)]
struct Args {
    #[clap(long)]
    dump_sections: bool,

    #[clap(subcommand)]
    commands: Commands,

    #[clap(short, long)]
    elf: String,

    #[clap(short, long)]
    sections: Vec<String>,
}

#[derive(Subcommand, Debug)]
enum Commands {
    Upload {
        serial_port: String,
    },
    SimulateWithPorts {
        serial_port: String,
        simulator_serial_port: String,
    },
    PrintSections,
    #[cfg(unix)]
    SimulateWithSocat,
}

#[cfg(unix)]
const MASTER_TTY: &str = "./master_tty";
#[cfg(unix)]
const SLAVE_TTY: &str = "./slave_tty";

fn main() {
    let args = Args::parse();
    let parser = match ElfParser::new(&args.elf) {
        Some(parser) => parser,
        None => return,
    };

    if let Commands::PrintSections = args.commands {
        parser.print_list_sections();
        return;
    }

    // If we are doing anything but printing sections, we need
    // at least one section.
    if args.sections.is_empty() {
        eprintln!("Please provide at least one section!");
        return;
    }

    // Check that every section specified exists.
    for section in &args.sections {
        match parser.get_section_header(section) {
            Some(_) => (),
            None => {
                eprintln!(
                    "Error: `{}` does not have a section named `{}`",
                    args.elf, section
                );
                return;
            }
        }
    }

    if args.dump_sections {
        for section in &args.sections {
            let header = parser.get_section_header(section).unwrap();
            let name = format!("{}.bin", section);

            if parser.dump_section(header, &name) {
                println!("Dumped {}", name);
            } else {
                eprintln!("Error: Unable to dump {}.", section);
            }
        }
    }

    match args.commands {
        Commands::Upload { serial_port } => {
            let port = match serial_port::open(&serial_port) {
                Ok(port) => port,
                Err(err) => {
                    eprintln!("Failed when trying to open `{}: {}.", serial_port, err);
                    return;
                }
            };
            if uploader::upload(port, parser, args.sections) {
                println!("Uploaded all sections successfully!");
            } else {
                eprintln!("Error on upload... flash is in an unknown state.");
            }
        }
        Commands::SimulateWithPorts {
            serial_port,
            simulator_serial_port,
        } => simulator::run(parser, args.sections, &serial_port, simulator_serial_port),
        #[cfg(unix)]
        Commands::SimulateWithSocat => {
            // TODO(José): Verify that socat is installed.
            let mut socat = Command::new("socat");
            socat
                .arg(format!("PTY,link={},raw,echo=0", MASTER_TTY))
                .arg(format!("PTY,link={},raw,echo=0", SLAVE_TTY))
                .stdout(Stdio::null())
                .stdin(Stdio::null())
                .stderr(Stdio::null());

            let _socat_guard = SocatProcessGuard::spawn(socat);
            simulator::run(parser, args.sections, MASTER_TTY, String::from(SLAVE_TTY));
        }
        _ => (),
    }
}

#[cfg(unix)]
struct SocatProcessGuard {
    socat: Child,
}

#[cfg(unix)]
impl SocatProcessGuard {
    fn spawn(mut command: Command) -> Self {
        let guard = Self {
            socat: command
                .spawn()
                .unwrap_or_else(|_| panic!("master: could not spawn `socat`")),
        };

        println!("master: Waiting for `socat`...");
        thread::sleep(Duration::from_millis(100));

        guard
    }
}

#[cfg(unix)]
impl Drop for SocatProcessGuard {
    fn drop(&mut self) {
        let panicking = thread::panicking();

        match self.socat.kill() {
            Ok(_) => {
                if !panicking {
                    println!("master: Terminating `socat`...")
                }
            }
            Err(e) => {
                if !panicking {
                    eprintln!("master: Could not terminate subprocess `socat`: {}.", e)
                }
            }
        }

        fs::remove_file(MASTER_TTY).unwrap_or(());
        fs::remove_file(SLAVE_TTY).unwrap_or(());
    }
}
