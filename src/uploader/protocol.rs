use std::{collections::VecDeque, fmt, time::Duration};

#[derive(PartialEq, Copy, Clone, Debug)]
pub enum RequestByte {
    /// Asks the firmware for the average page program time in milliseconds.
    /// The answer payload should be:
    /// [RequestByte::PageProgramTime, time_low, time_high]
    PageProgramTime = 1,

    /// Asks the firmware for the average page read time in milliseconds.
    /// The answer payload should be:
    /// [RequestByte::PageReadTime, time_low, time_high]
    PageReadTime,

    /// Asks the firmware for the average full erase time in milliseconds.
    /// The answer payload should be:
    /// [RequestByte::FullEraseTime, time_low, time_high]
    FullEraseTime,

    /// Asks the firmware for the size in bytes of each page of the flash.
    /// The answer payload should be:
    /// [RequestByte::PageSize, page_size_lo, page_size_hi]
    PageSize,

    /// Asks the firmware for the number of pages in the flash.
    /// The answer payload should be:
    /// [RequestByte::PageNumber, page_number[0], page_number[1], page_number[2], page_number[3]]
    PageNumber,

    /// Asks the firmware for the starting address of the flash.
    /// Note: This is the "virtual" address, the one to which the flash is mapped in the linker script.
    /// The answer payload should be:
    /// [RequestByte::FlashAddr, flash_addr[0], flash_addr[1], flash_addr[2], flash_addr[3]]
    FlashAddr,

    /// Asks the firmware to perform a mass erase in the flash.
    /// Note: This will wait an answer in up to the previously received "full erase time" milliseconds.
    /// The answer payload should be:
    /// [RequestByte::MassErase]
    MassErase,

    /// Asks the firmware to program a page of the flash:
    /// The sent payload is:
    /// [RequestByte::PageProgram, addr[0], addr[1], addr[2], addr[3], size[0], size[1], data[0], ..., data[size - 1]]
    /// Note 0: The first and last packets are the only that may contain less than "page size" bytes. The first packet
    /// may have a smaller size to align the writes to "page size" and the last packet may have a smaller size because
    /// there is not enough bytes left to fill a whole page. All other packets will contain page-size bytes of data and
    /// the address will always be at a page boundary.
    /// Note 1: If no answer is received in up to the previously received "page program time" milliseconds, another packet
    /// will be sent. This process can occur a maximum of 10 times before failure is detected.
    /// The answer payload should be:
    /// [RequestByte::PageProgram]
    PageProgram,

    /// Asks the firmware to calculate the u64 checksum of an specific address range.
    /// The sent payload is:
    /// [RequestByte::Checksum, start_addr[0], start_addr[1], start_addr[2], start_addr[3], end_addr[0], end_addr[1], end_addr[2], end_addr[3]]
    /// Note: This will wait up to the previously received "page read time" * "page number" milliseconds before failure is detected.
    /// The answer payload should be:
    /// [RequestByte::Checksum, checksum[0], checksum[1], checksum[2], checksum[3], checksum[4], checksum[5], checksum[6], checksum[7]]
    Checksum,
}

pub const DEFAULT_TIMEOUT_MS: u64 = 100;
pub const MAX_RETRIES_PER_PACKET: u32 = 10;

const HEADER: u8 = 0x5a;
const FOOTER: u8 = 0xa5;

fn calculate_checksum(payload: &[u8]) -> u16 {
    (!payload
        .iter()
        .map(|byte| *byte as u16)
        .reduce(|acc, value| acc.wrapping_add(value))
        .unwrap())
    .wrapping_add(1)
}

pub fn make_packet(payload: &[u8]) -> Vec<u8> {
    let payload_length = (payload.len() as u16).to_le_bytes();
    let checksum = calculate_checksum(payload).to_le_bytes();

    let mut packet = vec![HEADER];
    packet.push(payload_length[0]);
    packet.push(payload_length[1]);

    for byte in payload {
        packet.push(*byte);
    }

    packet.push(checksum[0]);
    packet.push(checksum[1]);
    packet.push(FOOTER);

    packet
}

#[derive(PartialEq)]
enum PacketDecodeState {
    LookingForHeader,
    LookingForSizeLo,
    LookingForSizeHi,
    LookingForPayload,
    LookingForChecksumLo,
    LookingForChecksumHi,
    LookingForFooter,
    Done,
}

pub struct PacketDecoder {
    state: PacketDecodeState,

    decoded_payload: Vec<u8>,
    temporary_storage: Vec<u8>,

    payload_size: u16,
    checksum: u16,
}

impl PacketDecoder {
    pub fn new() -> Self {
        Self {
            state: PacketDecodeState::LookingForHeader,
            decoded_payload: vec![],
            temporary_storage: vec![],
            payload_size: 0,
            checksum: 0,
        }
    }

    pub fn reset_state(&mut self) {
        self.state = PacketDecodeState::LookingForHeader;
    }

    pub fn try_decode(&mut self, queue: &mut VecDeque<u8>) -> Option<&Vec<u8>> {
        while self.state != PacketDecodeState::Done && !queue.is_empty() {
            let byte = queue.pop_front().unwrap();

            // We keep every byte except for the header in this storage,
            // so that when we know that we did not decode a packet properly
            // we can push_front() every byte in reverse order to the queue and
            // try decoding again with the byte that came after
            // the previously detected HEADER.
            if self.state != PacketDecodeState::LookingForHeader {
                self.temporary_storage.push(byte);
            }

            match self.state {
                PacketDecodeState::LookingForHeader => {
                    if byte == HEADER {
                        self.state = PacketDecodeState::LookingForSizeLo;
                    }
                }
                PacketDecodeState::LookingForSizeLo => {
                    self.payload_size = byte as u16;
                    self.state = PacketDecodeState::LookingForSizeHi;
                }
                PacketDecodeState::LookingForSizeHi => {
                    self.payload_size |= (byte as u16) << 8;
                    self.decoded_payload.clear();
                    self.state = PacketDecodeState::LookingForPayload;
                    // TODO(José): Check if the read size isn't abnormally big.
                    // That can happen if a byte equal to the HEADER is found but
                    // it's not actually a HEADER. We then read a bogus size and wait
                    // forever for a payload that we won't get.
                }
                PacketDecodeState::LookingForPayload => {
                    self.decoded_payload.push(byte);
                    if self.decoded_payload.len() == self.payload_size as usize {
                        self.state = PacketDecodeState::LookingForChecksumLo;
                    }
                }
                PacketDecodeState::LookingForChecksumLo => {
                    self.checksum = byte as u16;
                    self.state = PacketDecodeState::LookingForChecksumHi;
                }
                PacketDecodeState::LookingForChecksumHi => {
                    self.checksum |= (byte as u16) << 8;
                    let sum = self
                        .decoded_payload
                        .iter()
                        .map(|byte| *byte as u16)
                        .reduce(|sum, byte| sum.wrapping_add(byte))
                        .unwrap();
                    if self.checksum.wrapping_add(sum) == 0 {
                        self.state = PacketDecodeState::LookingForFooter;
                    } else {
                        self.handle_error(queue);
                    }
                }
                PacketDecodeState::LookingForFooter => {
                    if byte == FOOTER {
                        self.state = PacketDecodeState::Done;
                    } else {
                        self.handle_error(queue);
                    }
                }
                _ => (),
            }
        }

        if self.state == PacketDecodeState::Done {
            Some(&self.decoded_payload)
        } else {
            None
        }
    }

    fn handle_error(&mut self, queue: &mut VecDeque<u8>) {
        for byte in self.temporary_storage.iter().rev() {
            queue.push_front(*byte);
        }
        self.temporary_storage.clear();
        self.state = PacketDecodeState::LookingForHeader;
    }
}

#[derive(Debug)]
pub struct FlashInformation {
    page_program_time: Duration,
    page_read_time: Duration,
    full_erase_time: Duration,

    page_size: u16,
    page_number: u32,
    flash_size: u32,
    flash_addr: u32,
    flash_addr_end: u32,
}

impl FlashInformation {
    pub const fn zeroed() -> Self {
        Self {
            page_program_time: Duration::from_millis(0),
            page_read_time: Duration::from_millis(0),
            full_erase_time: Duration::from_millis(0),
            page_size: 0,
            page_number: 0,
            flash_size: 0,
            flash_addr: 0,
            flash_addr_end: 0,
        }
    }

    pub const fn set_page_program_time(mut self, page_program_time_ms: u16) -> Self {
        self.page_program_time = Duration::from_millis(page_program_time_ms as u64);
        self
    }

    pub const fn set_page_read_time(mut self, page_read_time_ms: u16) -> Self {
        self.page_read_time = Duration::from_millis(page_read_time_ms as u64);
        self
    }

    pub const fn set_full_erase_time(mut self, full_erase_time_ms: u16) -> Self {
        self.full_erase_time = Duration::from_millis(full_erase_time_ms as u64);
        self
    }

    pub const fn set_page_size(mut self, page_size: u16) -> Self {
        self.page_size = page_size;
        self.flash_size = self.page_number * self.page_size as u32;
        self
    }

    pub const fn set_page_number(mut self, page_number: u32) -> Self {
        self.page_number = page_number;
        self.flash_size = self.page_number * self.page_size as u32;
        self
    }

    pub const fn set_flash_addr(mut self, flash_addr: u32) -> Self {
        self.flash_addr = flash_addr;
        self.flash_addr_end = flash_addr + self.flash_size;
        self
    }

    pub fn page_program_time(&self) -> Duration {
        self.page_program_time
    }

    pub fn page_read_time(&self) -> Duration {
        self.page_read_time
    }

    pub fn full_erase_time(&self) -> Duration {
        self.full_erase_time
    }

    pub fn flash_addr(&self) -> u32 {
        self.flash_addr
    }

    pub fn flash_addr_end(&self) -> u32 {
        self.flash_addr_end
    }

    pub fn page_size(&self) -> u16 {
        self.page_size
    }

    pub fn page_number(&self) -> u32 {
        self.page_number
    }

    pub fn flash_size(&self) -> u32 {
        self.flash_size
    }
}

impl fmt::Display for FlashInformation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Flash Information")?;
        writeln!(f, "\tPage Program Time: {:#?}", self.page_program_time)?;
        writeln!(f, "\tPage Read Time: {:#?}", self.page_read_time)?;
        writeln!(f, "\tPage Full Erase Time: {:#?}", self.full_erase_time)?;
        writeln!(f, "\tPage Size: {} Bytes", self.page_size)?;
        writeln!(f, "\tPage Number: {}", self.page_number)?;
        writeln!(
            f,
            "\tFlash Size: {} Bytes {} KB {} MB",
            self.flash_size,
            self.flash_size / 1024,
            self.flash_size / 1024 / 1024
        )?;
        writeln!(f, "\tFlash Address: 0x{:08x}", self.flash_addr)?;
        write!(f, "\tFlash Address End: 0x{:08x}", self.flash_addr_end)
    }
}
