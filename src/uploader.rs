pub mod protocol;

// TODO(José): Work on errors, make sure we don't print anything in the inner
// layers, instead, return useful errors through enums.

use super::elf_parser::{ElfParser, SectionHeader, SectionType};
use progressing::{mapping::Bar as MappingBar, Baring};
use protocol::{FlashInformation, PacketDecoder, RequestByte};
use serial::SystemPort;
use std::{
    collections::VecDeque,
    io::Read,
    io::Write,
    time::{Duration, Instant},
};

pub fn upload(mut port: SystemPort, elf_parser: ElfParser, sections: Vec<String>) -> bool {
    let headers: Vec<_> = sections
        .iter()
        .map(|name| (name, elf_parser.get_section_header(name).unwrap()))
        .collect();

    for (name, header) in &headers {
        if header.sh_type == SectionType::NoBits {
            eprintln!("Section `{}` is of type {:?}. It cannot be uploaded because it has no contents on disk.", name, header.sh_type);
            return false;
        } else if header.size == 0 {
            eprintln!(
                "Section `{}` has size of 0 bytes. It cannot be uploaded!",
                name
            );
            return false;
        }
    }

    let flash_info = match upload_request_flash_info(&mut port) {
        Some(info) => {
            println!("{}", info);
            info
        }
        None => {
            eprintln!("Failed to get device information.");
            return false;
        }
    };

    for (name, header) in &headers {
        if header.vma < flash_info.flash_addr() as u32
            || header.vma_end > flash_info.flash_addr_end() as u32
        {
            eprintln!(
                "`{}` is either not located in flash or doesn't fit in it.",
                name
            );
            eprintln!("Flash Base Address: 0x{:08x} Flash End Address: 0x{:08x} Section Base Address: 0x{:08x} Section End Address: 0x{:08x}", flash_info.flash_addr(), flash_info.flash_addr_end(), header.vma, header.vma_end);
            return false;
        }
    }

    // TODO(José): Enable other kinds of erasure and add flag
    // to skip erasing.
    println!("Requesting full erase...");
    if !upload_mass_erase_request(&mut port, &flash_info) {
        println!("Not able to erase flash!");
        return false;
    }
    println!("Erased!");

    for (name, header) in &headers {
        if !upload_section(&mut port, header, &elf_parser, &flash_info, name) {
            eprintln!("\nError when trying to upload section: {}", name);
            return false;
        }
    }

    for (name, header) in &headers {
        println!("Verifying checksum for section {}...", name);
        match upload_request_checksum(&mut port, header, &flash_info) {
            Some(checksum) => {
                let section_data = elf_parser
                    .read_section(header, header.size as usize, 0)
                    .unwrap();
                assert_eq!(section_data.len(), header.size as usize);

                let local_checksum = (!section_data
                    .iter()
                    .map(|byte| *byte as u64)
                    .reduce(|acc, value| acc.wrapping_add(value))
                    .unwrap())
                .wrapping_add(1);

                if local_checksum == checksum {
                    println!(" Valid checksum!");
                } else {
                    eprintln!(
                        " Checksums don't match - local checksum: {:16x} received checksum: {:16x}",
                        local_checksum, checksum
                    );
                }
            }
            None => {
                eprintln!(" Unable to receive checksum.");
            }
        }
    }

    true
}

fn upload_request_flash_info(port: &mut SystemPort) -> Option<FlashInformation> {
    let mut page_program_time_ms = 0;
    let mut page_read_time_ms = 0;
    let mut full_erase_time_ms = 0;
    let mut page_size = 0;
    let mut page_number = 0;
    let mut flash_addr = 0;

    let mut finished = false;
    let mut requesting_now = RequestByte::PageProgramTime;

    while !finished {
        print!("Asking for {:?}... ", requesting_now);

        let payload = [requesting_now as u8];
        let packet = protocol::make_packet(&payload);

        let mut retries = 0;
        while retries != protocol::MAX_RETRIES_PER_PACKET {
            let answer = match send_and_wait_answer(
                &packet,
                port,
                Duration::from_millis(protocol::DEFAULT_TIMEOUT_MS),
            ) {
                Some(answer) => answer,
                None => {
                    retries += 1;
                    continue;
                }
            };

            if answer[0] != requesting_now as u8 {
                retries += 1;
                continue;
            }

            // TODO(José): Verify sizes.
            match requesting_now {
                RequestByte::PageProgramTime => {
                    page_program_time_ms = u16::from_le_bytes(answer[1..3].try_into().unwrap());
                    requesting_now = RequestByte::PageReadTime;
                }
                RequestByte::PageReadTime => {
                    page_read_time_ms = u16::from_le_bytes(answer[1..3].try_into().unwrap());
                    requesting_now = RequestByte::FullEraseTime;
                }
                RequestByte::FullEraseTime => {
                    full_erase_time_ms = u16::from_le_bytes(answer[1..3].try_into().unwrap());
                    requesting_now = RequestByte::PageSize;
                }
                RequestByte::PageSize => {
                    page_size = u16::from_le_bytes(answer[1..3].try_into().unwrap());
                    requesting_now = RequestByte::PageNumber;
                }
                RequestByte::PageNumber => {
                    page_number = u32::from_le_bytes(answer[1..5].try_into().unwrap());
                    requesting_now = RequestByte::FlashAddr;
                }
                RequestByte::FlashAddr => {
                    flash_addr = u32::from_le_bytes(answer[1..5].try_into().unwrap());
                    finished = true;
                }
                _ => (),
            }

            println!("Ok!");
            break;
        }

        if retries == protocol::MAX_RETRIES_PER_PACKET {
            return None;
        }
    }

    Some(
        FlashInformation::zeroed()
            .set_page_program_time(page_program_time_ms)
            .set_page_read_time(page_read_time_ms)
            .set_full_erase_time(full_erase_time_ms)
            .set_page_size(page_size)
            .set_page_number(page_number)
            .set_flash_addr(flash_addr),
    )
}

fn upload_mass_erase_request(port: &mut SystemPort, flash_info: &FlashInformation) -> bool {
    let packet = protocol::make_packet(&[RequestByte::MassErase as u8]);

    let mut retries = 0;
    while retries != protocol::MAX_RETRIES_PER_PACKET {
        let answer = match send_and_wait_answer(&packet, port, flash_info.full_erase_time()) {
            Some(answer) => answer,
            None => {
                retries += 1;
                continue;
            }
        };

        if answer[0] == RequestByte::MassErase as u8 {
            return true;
        }

        retries += 1;
    }

    false
}

fn upload_section(
    port: &mut SystemPort,
    header: &SectionHeader,
    elf_parser: &ElfParser,
    flash_info: &FlashInformation,
    name: &String,
) -> bool {
    let make_payload = |address: u32, size: u16, data: &[u8]| {
        let address = address.to_le_bytes();
        let size = size.to_le_bytes();

        let mut payload = vec![RequestByte::PageProgram as u8];

        for byte in address {
            payload.push(byte);
        }

        for byte in size {
            payload.push(byte);
        }

        for byte in data {
            payload.push(*byte);
        }

        payload
    };

    let mut send_page = |address: u32, size: u16, data: &[u8]| {
        let packet = protocol::make_packet(&make_payload(address, size, data));
        let mut retries = 0;

        while retries != protocol::MAX_RETRIES_PER_PACKET {
            let answer = match send_and_wait_answer(&packet, port, flash_info.page_program_time()) {
                Some(answer) => answer,
                None => {
                    retries += 1;
                    continue;
                }
            };

            if answer[0] == RequestByte::PageProgram as u8 {
                return true;
            }

            retries += 1;
        }

        false
    };

    let page_size = flash_info.page_size() as u32;

    let mut address = header.vma;
    let miss_aligned_bytes = address % page_size;

    let total_pages = (header.size as f64 / page_size as f64).ceil() as u32;
    let mut progress_bar = MappingBar::with_range(0, total_pages as i32).timed();

    if miss_aligned_bytes != 0 {
        let size = page_size - miss_aligned_bytes;

        let data = elf_parser.read_section(header, size as usize, 0);
        if data.is_none() {
            return false;
        }

        let data = data.unwrap();
        assert_eq!(data.len(), size as usize);

        if !send_page(address, size as u16, data) {
            return false;
        }

        address += size;

        progress_bar.add(1);
        print!("\r{}: {}", name, progress_bar);
        std::io::stdout().flush().unwrap();
    }

    if address >= header.vma_end {
        return true;
    }

    while address < header.vma_end {
        let diff = header.vma_end - address;
        let size = if diff >= page_size { page_size } else { diff };
        let off = address - header.vma;

        let data = elf_parser.read_section(header, size as usize, off as usize);
        if data.is_none() {
            return false;
        }

        let data = data.unwrap();
        assert_eq!(data.len(), size as usize);

        if !send_page(address, size as u16, data) {
            return false;
        }

        address += size;
        progress_bar.add(1);
        print!("\r{}: {}", name, progress_bar);
        std::io::stdout().flush().unwrap();
    }

    println!();
    true
}

fn upload_request_checksum(
    port: &mut SystemPort,
    header: &SectionHeader,
    flash_info: &FlashInformation,
) -> Option<u64> {
    let section_page_number = (header.size as f64 / flash_info.page_size() as f64).ceil() as u32;
    let timeout = flash_info.page_read_time() * section_page_number;

    let start_address = header.vma.to_le_bytes();
    let end_address = header.vma_end.to_le_bytes();

    let mut payload = vec![RequestByte::Checksum as u8];

    for byte in start_address {
        payload.push(byte);
    }

    for byte in end_address {
        payload.push(byte);
    }

    let packet = protocol::make_packet(&payload);
    let mut retries = 0;
    while retries != protocol::MAX_RETRIES_PER_PACKET {
        let answer = match send_and_wait_answer(&packet, port, timeout) {
            Some(answer) => answer,
            None => {
                retries += 1;
                continue;
            }
        };

        if answer[0] == RequestByte::Checksum as u8 {
            return Some(u64::from_le_bytes(answer[1..9].try_into().unwrap()));
        }

        retries += 1;
    }

    None
}

fn send_and_wait_answer(
    packet: &[u8],
    port: &mut SystemPort,
    timeout: Duration,
) -> Option<Vec<u8>> {
    let mut queue = VecDeque::new();
    let mut rx_buffer: [u8; 32] = [0; 32];
    let mut decoder = PacketDecoder::new();

    assert_eq!(port.write(packet).unwrap(), packet.len());

    let now = Instant::now();
    while now.elapsed() < timeout {
        if let Ok(n) = port.read(&mut rx_buffer) {
            for byte in rx_buffer.iter().take(n) {
                queue.push_back(*byte);
            }
        }

        if let Some(payload) = decoder.try_decode(&mut queue) {
            return Some(payload.clone());
        }
    }

    None
}
