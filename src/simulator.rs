use super::{
    uploader::{
        self,
        protocol::{self, FlashInformation, PacketDecoder, RequestByte},
    },
    ElfParser,
};
use serial::SystemPort;
use std::{
    collections::VecDeque,
    io::{Read, Write},
    sync::mpsc::{self, Receiver, Sender},
    thread,
};

use super::serial_port;

const SIMULATOR_FLASH_INFO: FlashInformation = FlashInformation::zeroed()
    .set_page_program_time(20)
    .set_page_read_time(20)
    .set_full_erase_time(1000)
    .set_page_size(256)
    .set_page_number(32768)
    .set_flash_addr(0x9000_0000);

pub fn run(
    parser: ElfParser,
    sections: Vec<String>,
    serial_port: &str,
    simulator_serial_port: String,
) {
    let (tx, simulator_rx) = mpsc::channel::<()>();
    let (simulator_tx, rx) = mpsc::channel::<bool>();

    let simulator_thread = thread::spawn(move || {
        sim_thread_main(&simulator_serial_port, simulator_tx, simulator_rx);
    });

    println!("master: Waiting for simulator to finish setting up!");
    if !rx.recv().unwrap() {
        eprintln!("master: Error: Simulator failed to set up...");
        return;
    }

    let port = match serial_port::open(serial_port) {
        Ok(port) => port,
        Err(err) => {
            eprintln!(
                "master: Failed when trying to open `{}: {}.",
                serial_port, err
            );
            tx.send(()).unwrap();
            simulator_thread.join().unwrap();
            return;
        }
    };

    if uploader::upload(port, parser, sections) {
        println!("Uploaded all sections successfully!");
    } else {
        eprintln!("Error on upload... flash is in an unknown state.");
    }

    tx.send(()).unwrap();
    simulator_thread.join().unwrap();
}

fn sim_thread_main(port_path: &str, tx: Sender<bool>, rx: Receiver<()>) {
    let mut port = match serial_port::open(port_path) {
        Ok(port) => port,
        Err(err) => {
            eprintln!(
                "simulator: Error when trying to open `{}`: {}.",
                port_path, err
            );
            tx.send(false).unwrap();
            return;
        }
    };

    let mut memory = vec![0xa5; SIMULATOR_FLASH_INFO.flash_size() as usize];

    let mut queue = VecDeque::new();
    let mut rx_buffer: [u8; 512] = [0; 512];
    let mut decoder = PacketDecoder::new();

    // Tell the main thread that we've finished setting up...
    tx.send(true).unwrap();
    loop {
        // Check if main thread is finished so that we can break out of the loop.
        if rx.try_recv().is_ok() {
            break;
        }

        if let Ok(n) = port.read(&mut rx_buffer) {
            for byte in rx_buffer.iter().take(n) {
                queue.push_back(*byte);
            }
        }

        if let Some(payload) = decoder.try_decode(&mut queue) {
            execute_and_answer(payload, &mut memory, &mut port);
            decoder.reset_state();
        }
    }

    println!("simulator: exiting...");
}

fn execute_and_answer(payload: &[u8], memory: &mut [u8], port: &mut SystemPort) {
    let packet = if payload[0] == (RequestByte::PageProgramTime as u8) {
        let time = (SIMULATOR_FLASH_INFO.page_program_time().as_millis() as u16).to_le_bytes();
        protocol::make_packet(&[RequestByte::PageProgramTime as u8, time[0], time[1]])
    } else if payload[0] == (RequestByte::PageReadTime as u8) {
        let time = (SIMULATOR_FLASH_INFO.page_read_time().as_millis() as u16).to_le_bytes();
        protocol::make_packet(&[RequestByte::PageReadTime as u8, time[0], time[1]])
    } else if payload[0] == (RequestByte::FullEraseTime as u8) {
        let time = (SIMULATOR_FLASH_INFO.full_erase_time().as_millis() as u16).to_le_bytes();
        protocol::make_packet(&[RequestByte::FullEraseTime as u8, time[0], time[1]])
    } else if payload[0] == (RequestByte::PageSize as u8) {
        let size = SIMULATOR_FLASH_INFO.page_size().to_le_bytes();
        protocol::make_packet(&[RequestByte::PageSize as u8, size[0], size[1]])
    } else if payload[0] == (RequestByte::PageNumber as u8) {
        let number = SIMULATOR_FLASH_INFO.page_number().to_le_bytes();
        protocol::make_packet(&[
            RequestByte::PageNumber as u8,
            number[0],
            number[1],
            number[2],
            number[3],
        ])
    } else if payload[0] == (RequestByte::FlashAddr as u8) {
        let address = SIMULATOR_FLASH_INFO.flash_addr().to_le_bytes();
        protocol::make_packet(&[
            RequestByte::FlashAddr as u8,
            address[0],
            address[1],
            address[2],
            address[3],
        ])
    } else if payload[0] == (RequestByte::MassErase as u8) {
        for byte in memory {
            *byte = 0xff;
        }
        protocol::make_packet(&[RequestByte::MassErase as u8])
    } else if payload[0] == (RequestByte::PageProgram as u8) {
        let address = u32::from_le_bytes(payload[1..5].try_into().unwrap())
            - SIMULATOR_FLASH_INFO.flash_addr();
        let size = u16::from_le_bytes(payload[5..7].try_into().unwrap());
        let data = &payload[7..7 + size as usize];

        for i in 0..size {
            let current_address = address as usize + i as usize;
            memory[current_address] = (!memory[current_address]) | data[i as usize];
        }

        protocol::make_packet(&[RequestByte::PageProgram as u8])
    } else if payload[0] == (RequestByte::Checksum as u8) {
        let start_address = (u32::from_le_bytes(payload[1..5].try_into().unwrap())
            - SIMULATOR_FLASH_INFO.flash_addr()) as usize;
        let end_address = (u32::from_le_bytes(payload[5..9].try_into().unwrap())
            - SIMULATOR_FLASH_INFO.flash_addr()) as usize;

        let cs = (!memory[start_address..end_address]
            .iter()
            .map(|byte| *byte as u64)
            .reduce(|acc, value| acc.wrapping_add(value))
            .unwrap())
        .wrapping_add(1)
        .to_le_bytes();

        protocol::make_packet(&[
            RequestByte::Checksum as u8,
            cs[0],
            cs[1],
            cs[2],
            cs[3],
            cs[4],
            cs[5],
            cs[6],
            cs[7],
        ])
    } else {
        return;
    };
    assert_eq!(port.write(&packet).unwrap(), packet.len());
}
