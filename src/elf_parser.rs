use std::{cmp::Ordering, collections::HashMap, fs};

struct ElfHeader {
    e_shoff: u64,
    e_shentsize: u16,
    e_shnum: u16,
    e_shstrndx: u16,
}

#[derive(Debug, PartialEq)]
pub enum SectionType {
    Null,
    ProgBits,
    SymTab,
    StrTab,
    Rela,
    Hash,
    Dynamic,
    Note,
    NoBits,
    Rel,
    ShLib,
    DynSym,
    InitArray,
    FiniArray,
    PreInitArray,
    Group,
    SymTabShndx,
    ShtNum,
}

impl From<u32> for SectionType {
    fn from(value: u32) -> Self {
        match value {
            0x00 => Self::Null,
            0x01 => Self::ProgBits,
            0x02 => Self::SymTab,
            0x03 => Self::StrTab,
            0x04 => Self::Rela,
            0x05 => Self::Hash,
            0x06 => Self::Dynamic,
            0x07 => Self::Note,
            0x08 => Self::NoBits,
            0x09 => Self::Rel,
            0x0a => Self::ShLib,
            0x0b => Self::DynSym,
            0x0e => Self::InitArray,
            0x0f => Self::FiniArray,
            0x10 => Self::PreInitArray,
            0x11 => Self::Group,
            0x12 => Self::SymTabShndx,
            0x13 => Self::ShtNum,
            _ => Self::Null,
        }
    }
}

pub struct SectionHeader {
    pub vma: u32,
    pub vma_end: u32,
    off: u32,
    pub size: u32,
    name_off: u32,
    pub sh_type: SectionType,
}

pub struct ElfParser {
    data: Vec<u8>,
    sections: HashMap<String, SectionHeader>,
}

fn read_le_u32_at_offset(data: &[u8], offset: usize) -> u32 {
    u32::from_le_bytes(data[offset..offset + 4].try_into().unwrap())
}

fn read_le_u16_at_offset(data: &[u8], offset: usize) -> u16 {
    u16::from_le_bytes(data[offset..offset + 2].try_into().unwrap())
}

impl ElfParser {
    pub fn new(path_name: &str) -> Option<Self> {
        let data = match fs::read(path_name) {
            Ok(data) => data,
            Err(err) => {
                eprintln!("Error when trying to open {}: {}", path_name, err);
                return None;
            }
        };

        if !Self::is_magic_valid(&data) {
            return None;
        }

        let elf_header = match Self::read_elf_header(&data) {
            Some(header) => header,
            None => return None,
        };

        let sections = match Self::read_section_headers(&data, &elf_header) {
            Some(sections) => sections,
            None => return None,
        };

        Some(ElfParser { data, sections })
    }

    pub fn get_section_header(&self, name: &str) -> Option<&SectionHeader> {
        self.sections.get(name)
    }

    pub fn read_section(&self, header: &SectionHeader, size: usize, off: usize) -> Option<&[u8]> {
        assert!(size != 0);

        // Out of section bounds read.
        if size + off > header.size as usize {
            return None;
        }

        // Out of file bounds read.
        let file_offset = header.off as usize + off;
        if file_offset + size > self.data.len() {
            return None;
        }

        Some(&self.data[file_offset..file_offset + size])
    }

    pub fn dump_section(&self, header: &SectionHeader, path_name: &str) -> bool {
        if header.sh_type == SectionType::NoBits {
            return false;
        }

        let off = header.off as usize;
        let size = header.size as usize;

        matches!(fs::write(path_name, &self.data[off..off + size]), Ok(_))
    }

    pub fn print_list_sections(&self) {
        // ELF sections are named in ASCII, we know that the length is equal to the number
        // of characters.

        let mut sections: Vec<_> = self.sections.iter().collect();
        sections.sort_unstable_by(|a, b| {
            let a = a.1;
            let b = b.1;

            if a.vma < b.vma {
                Ordering::Less
            } else if a.vma > b.vma {
                Ordering::Greater
            } else if a.off < b.off {
                Ordering::Less
            } else if a.off > b.off {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        });

        let greatest_length = sections
            .iter()
            .map(|name_and_header| {
                let name = name_and_header.0;
                name.len()
            })
            .reduce(
                |greatest, length| {
                    if length > greatest {
                        length
                    } else {
                        greatest
                    }
                },
            )
            .unwrap();

        for (name, header) in sections.iter() {
            let diff = greatest_length - name.len();
            print!("Section: {}", name);
            for _ in 0..diff {
                print!(" ");
            }
            println!(
                " VMA: {:08x} Size: {:08x} Type: {:?}",
                header.vma, header.size, header.sh_type
            );
        }
    }

    fn is_magic_valid(data: &[u8]) -> bool {
        if data.len() > 4 {
            let magic = read_le_u32_at_offset(data, 0);
            magic == 0x464c457f
        } else {
            eprintln!("Error: Not big enough to have MAGIC.");
            false
        }
    }

    fn read_elf_header(data: &[u8]) -> Option<ElfHeader> {
        if data.len() < 52 {
            eprintln!("Error: Not big enough to have elf file header.");
            return None;
        }

        if data[4] != 1 {
            eprintln!("Error: Non 32-bit elf detected.");
            return None;
        }

        Some(ElfHeader {
            e_shoff: read_le_u32_at_offset(data, 0x20) as u64,
            e_shentsize: read_le_u16_at_offset(data, 0x2e),
            e_shnum: read_le_u16_at_offset(data, 0x30),
            e_shstrndx: read_le_u16_at_offset(data, 0x32),
        })
    }

    fn read_section_headers(
        data: &[u8],
        elf_header: &ElfHeader,
    ) -> Option<HashMap<String, SectionHeader>> {
        let mut section_headers = vec![];

        for i in 0..elf_header.e_shnum as usize {
            let offset = elf_header.e_shoff as usize + (i * elf_header.e_shentsize as usize);

            let name_off = read_le_u32_at_offset(data, offset);
            let sh_type = read_le_u32_at_offset(data, offset + 0x04);
            let vma = read_le_u32_at_offset(data, offset + 0x0c);
            let off = read_le_u32_at_offset(data, offset + 0x10);
            let size = read_le_u32_at_offset(data, offset + 0x14);
            let vma_end = vma + size;

            section_headers.push(SectionHeader {
                vma,
                vma_end,
                off,
                size,
                name_off,
                sh_type: sh_type.into(),
            });
        }

        let mut sections = HashMap::new();

        let strtab_off = section_headers[elf_header.e_shstrndx as usize].off as usize;
        for header in section_headers {
            let mut offset = strtab_off + header.name_off as usize;
            let mut name = String::new();
            while data[offset] != 0 {
                name.push(data[offset] as char);
                offset += 1;
            }

            sections.insert(name, header);
        }

        Some(sections)
    }
}
